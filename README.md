A python project to play around with and get to understand *how* the packaging ecosystem of python works. Heavily inspired from 

https://github.com/pypa/sampleproject

and 

https://packaging.python.org/en/latest/tutorials/packaging-projects/
