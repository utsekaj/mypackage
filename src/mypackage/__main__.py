import os
import pkgutil
import mypackage.lib.a
import mypackage.lib as lib
import mypackage.sim.b
import mypackage.sim as sim

import mypackage.a as a
import mypackage.b as b

def main():
    print(__file__)
    _pkgroot = os.path.dirname(os.path.abspath(__file__))
    _pkgdata = os.path.join(_pkgroot, 'data')
    print('how do you like me now')
    lib.a.call()
    sim.b.call()
    a.call()
    b.call()

    # Why this way? How to deal with bytes objects as opposed to file?
    # https://stackoverflow.com/a/58941536
    # test_data_file = pkgutil.get_data(__name__, 'data/test.txt')
    #  print(test_data_file)
    test_data_file = os.path.join(_pkgdata, 'test.txt')
    with open(test_data_file, 'r') as f:
        print(f.read())
    # tmp = io.StringIO(test_data_file)
    # print(tmp.read())
